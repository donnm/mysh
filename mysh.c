/* mysh Simple shell
 *
 * Donn Morrison 2019
 *
 * License: Public domain
 *
 * TODO:
 * - unlimited command line arguments
 * - environment variables
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

#define MAX_INPUT 256
extern int errno;

char *search_path(char *cmd)
{
    char *entry=NULL;
    char fullpath[MAX_INPUT];
    struct stat buf;
    char *path=malloc(strlen(getenv("PATH"))+1);
    char *saveptr;
    if(path == NULL)
        return NULL;
    strncpy(path, getenv("PATH"), strlen(getenv("PATH")));
    entry = strtok_r(path, ":", &saveptr);
    while(entry != NULL)
    {
        snprintf(fullpath, MAX_INPUT-1, "%s/%s", entry, cmd);
        if(stat(fullpath, &buf) == 0)
        {
            // Effectively freed after execve is called (heap is reset)
            char *foundentry = malloc(strlen(cmd)+1+strlen(entry)+1); // +1 /, +1 \0
            snprintf(foundentry, strlen(cmd)+1+strlen(entry)+1, "%s/%s", entry, cmd);
            return foundentry;
        }
        entry = strtok_r(NULL, ":", &saveptr);
    }

    return NULL;
}

int get_input(char *buffer, int maxlen)
{
        // Show a prompt
        printf("$> ");

        // Read input (fgets reserves one byte for '\0')
        if(fgets(buffer, maxlen, stdin) == NULL)
            return -1;

        // Remove trailing newline (user presses enter)
        if(buffer[strlen(buffer)-1] == '\n')
            buffer[strlen(buffer)-1]='\0';

        return strlen(buffer);
}

int main ()
{
    int i=0, pid;
    char cmd[MAX_INPUT];
    int cmdlen;
    int status;

    while(1)
    {
        cmdlen = get_input(cmd, MAX_INPUT);
        if(cmdlen == -1)
        {
            // Exit on ctrl-d
            printf("Exiting\n");
            exit(0);
        }
        else if(cmdlen == 0)
            // User just pressed enter, show prompt again
            continue;

        // Fork a new process
        pid = fork();
        if(pid == -1)
        {
            printf("Couldn't fork, exiting.\n");
            exit(1);
        }
        if(pid==0)
        {
            // Child process, execute the command
            char *args[3];
            char *saveptr;
            // TODO pass path
            char *env[3] = {"PWD=/bin", "DISPLAY=:0", NULL};
            char *fullpathcmd = search_path(strtok_r(cmd, " ", &saveptr));
            if(fullpathcmd == NULL)
            {
                printf("Command not found\n");
                exit(1);
            }
            args[0]=fullpathcmd;
            args[1]=strtok_r(NULL," ",&saveptr);
            args[2]=NULL;
            int ret = execve(args[0], args, NULL);
            // Will only return here on error
            printf("Bad command %s, execve() failed, errno = %d\n", args[0], errno);
            perror("execve");
        }
        else
        {
            // Parent process, wait for child to exit
            while(wait(&status) != pid);
        }
    }
}
