# mysh

Simple shell written in C

## Features

- fork, exec, wait
- PATH support
- one command line argument

## Compile

`gcc mysh.c -o mysh`